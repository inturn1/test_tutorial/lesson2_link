import Layout from "../components/layout"
import Head from 'next/head';
import Link from "next/link";
import styles from '../components/layout.module.css';


export default function login1(){
  return (
    <>
      <div>
        <>
        <Head>
          <title>Login Page</title>
        </Head>
        <h1>Welcome To Login 1 !!!</h1>
        <div className={styles.backToHome}>
          <li>
          <Link href="/posts/first-post"> ⬅ Back to first post</Link><br/>
          <Link href="/alert"> Alert... </Link><br/>
          </li>
        </div>
        </>
      </div>
    </>  
    );
}
