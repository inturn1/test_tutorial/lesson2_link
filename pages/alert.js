import styles from '../components/alert.module.css';
import classNames from 'classnames/bind';
let cn = classNames.bind(styles)


export default function Alert({ children, type }) {
  return (
    <div
      className={cn({
        [styles.success]: type === 'success',
        [styles.error]: type === 'error',
      })}
    >
      {children}
    </div>
  );
}