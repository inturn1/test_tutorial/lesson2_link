import Link from 'next/link'
import Head from 'next/head';
import Script from 'next/script';
import Layout from '../../components/layout';



// Link Tag can load page quickly more a Tag
// Link go next page not refresh

export default function FirstPost() {
  return (
    <Layout>
        <Head>
          <title>First Post</title>
          <script src="https://connect.facebook.net/en_US/sdk.js" />
        </Head>
        <Script
          src="https://connect.facebook.net/en_US/sdk.js"
          strategy='lazyOnload'
          onLoad={() => 
            console.log('script loaded correctly, window.FB has been populated')
          }
          />
        <h1>First Post</h1>
    </Layout>
  );
}

// function Home() {
//   return (
//     <ul>
//       <li>
//         <Link href="/">Home</Link>
//       </li>
//       <li>
//         <Link href="/about">About Us</Link>
//       </li>
//       <li>
//         <Link href="/blog/hello-world">Blog Post</Link>
//       </li>
//     </ul>
//   )
// }

